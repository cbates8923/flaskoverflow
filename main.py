from flask import Flask, render_template, redirect, request
import sqlite3 as sqlite

app = Flask(__name__)

conn = sqlite.connect("database.db")
cursor = conn.cursor()

cursor.execute("")

@app.route('/', methods=["POST", "GET"])
def main():
    if request.method == "POST":
        if request.form.get('Create') == 'Create':
            return redirect("/create")
    
    return render_template("index.html")

@app.route('/create')
def create_account():
    return render_template("create.html")